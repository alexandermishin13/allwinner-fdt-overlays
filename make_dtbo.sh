#!/bin/sh

/bin/ls -R1 ./src | grep -e \.dtso\$ | while read _file;
do
_filename="${_file%%.*}"
/usr/bin/dtc -@ -I dts -O dtb -o ./dtb/${_filename}.dtbo ./src/${_file}
done
