# allwinner-fdt-overlays

Allwinner FDT overlays collection for FreeBSD

## About

It is a collection of FDT overlays for Allwinner based SoC platforms.

## Description

All files are grouped in several directories:
* **./src/** - Source code of overlays light and simple to compile;
* **./dts/** - Source code of overlays with dependencies.
Put ones You need into `/usr/src/sys/dts/arm/overlays/` and run `make` right in there.
Blobs would be placed into `/tmp/` with `.dtbo` extention;
* **./dtb/** - Precompiled blobs ready to use. Need You them?

Put the blobs into `/boot/dtb/overlays/` directory and enable them in
`/boot/loader.conf` as follows:
```
fdt_overlays="sun8i-h2-plus-sid,sun8i-h2-plus-ths,sun8i-h2-plus-iic0,sun8i-h2-plus-w1-gpio"
```
List of FDT-overlays:
* **sun8i-&lt;soc&gt;-bh1750-i2c0.dtso** - Ambient Light Sensor `bh1750fvi`
[bh1750-kmod](https://gitlab.com/alexandermishin13/bh1750-kmod.git)
* **sun8i-&lt;soc&gt;-dht11-gpio.dtso** - Temperature-Humidity Sensor `gpioths(4)`
* **sun8i-h2-plus-iic0.dtso** - `iicbus(4) 0` (not in the base for `h2+`)
* **sun8i-h2-plus-iic1.dtso** - `iicbus(4) 1` (not in the base for `h2+`)
* **sun8i-&lt;soc&gt;-sid.dtso** - `aw_sid(4)` (already in the base)
* **sun8i-&lt;soc&gt;-tm1637-gpio.dtso** - 4 digit 7 segment display `tm1637`
[tm1637-kmod](https://gitlab.com/alexandermishin13/tm1637-kmod.git)
* **sun8i-&lt;soc&gt;-w1-gpio.dtso** - One wire bus `ow(4)`
* **sun8i-&lt;soc&gt;-ths.dtso** - Temperature sensor `aw_thermal(4)`
(already in base), `aw_sid(4)` is required
